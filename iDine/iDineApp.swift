//
//  iDineApp.swift
//  iDine
//
//  Created by Xavier Mellado Esteban on 23/9/23.
//

import SwiftUI

@main
struct iDineApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
